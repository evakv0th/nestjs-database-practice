import { Controller, Get } from '@nestjs/common';
import { UsersService } from './users.service';
import { User } from './entites/user.entities';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  async findAllUsers(): Promise<User[]> {
    return this.usersService.findAll();
  }
}
